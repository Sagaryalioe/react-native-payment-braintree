import renderIf from "render-if";
import dropin from "braintree-web-drop-in";
import glamorous from "glamorous";
import RNMessageChannel from "react-native-webview-messaging";
// import { connectToRemote } from 'react-native-webview-messaging/web'
import React from "react";
import PropTypes from "prop-types";
const util = require("util");

import {
  RETRIEVE_NONCE_PENDING,
  RETRIEVE_NONCE_FULFILLED,
  RETRIEVE_NONCE_REJECTED,
  TOKEN_RECEIVED,
  PAYMENT_FULFILLED,
  PAYMENT_REJECTED,
  PAYMENT_SUCCESS
} from "./constants";

const DropInContainer = glamorous.div({
  flex: 1
});
const ButtonContainer = glamorous.div({
  flex: 1
});
const Button = glamorous.div({
  borderRadius: "2px",
  padding: "2px 10px 2px 10px",
  backgroundColor: "#2ecc71",
  fontSize: "1.25em",
  color: "white",
  fontFamily: "arial",
  boxShadow: "0 1px 4px rgba(0, 0, 0, .6)",
  textAlign: "center"
});
const PaymentBackground = glamorous.div({
  position: "absolute",
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  display: "flex",
  flexDirection: "column"
});

// print passed information in an html element; useful for debugging
// since console.log and debug statements won't work in a conventional way
const PrintElement = data => {
  if (typeof data === "object") {
    let el = document.createElement("pre");
    el.innerHTML = util.inspect(data, { showHidden: false, depth: null });
    document.getElementById("messages").appendChild(el);
  } else if (typeof data === "string") {
    let el = document.createElement("pre");
    el.innerHTML = data;
    document.getElementById("messages").appendChild(el);
  }
};

export default class BraintreeReactHTML extends React.Component {
  constructor() {
    super();
    this.state = {
      currentPaymentStatus: null,
      instance: null
    };
  }

  componentDidMount = () => {
    // PrintElement("componentDidMount success");
    console.log('ciompinne didmount listernenrksdf')
    this.registerMessageListeners();
  };

  /*******************************
   * register message listeners to receive events from parent
   */
  registerMessageListeners = () => {
    // PrintElement("registering message listeners");

    // will receive client token as a prop immediately upon mounting

    console.log('register message listerner outside');

    // connectToRemote()
    //   .then(function(remote){
    //     console.log('connect to remote')
      
    //     remote.on(TOKEN_RECEIVED, function(event){
    //       //PrintElement(event);
    //       // if (event.payload.options.creditCard) {

    //       console.log('register message listerner inside: ', event)

    //       this.createCreditCardUI(event.payload.clientToken);
    //       /*  }
    //       if (event.payload.options.paypal) {
    //         this.createPaypalUI(event.payload.clientToken);
    //       } */
    //     });


    //     // when the call is made to the braintree purchasing server
    //     // used to show the user some feedback that the purchase is in process
    //     remote.on("PAYMENT_PENDING", function(event){
    //       // PrintElement("PAYMENT_PENDING");
    //       this.setState({ currentPaymentStatus: PAYMENT_FULFILLED });
    //     });

    //     // when the purchase succeeds
    //     // used to show the user some feedback that the purchase has completed successfully
    //     remote.on(PAYMENT_FULFILLED, function(event){
    //       //PrintElement(PAYMENT_FULFILLED);
    //       this.setState({ currentPaymentStatus: PAYMENT_FULFILLED });
    //     });

    //     // when the purchase succeeds
    //     // used to show the user some feedback that the purchase has failed to complete
    //     remote.on(PAYMENT_REJECTED, function(event){
    //       this.setState({ currentPaymentStatus: PAYMENT_REJECTED });
    //       PrintElement(PAYMENT_REJECTED);
    //     });

    //     remote.emit('custom-event-from-webview', { payload: 'Custom event from WebView' });
    //   });

    // this.createCreditCardUI("eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIzOGExNTVhZDRlZDIzNjgwMzBlNmY2OTM4Mjc0MWE4NzJjZTdhMTdjZGMyODVmYzRhYjBiZmYzZWY5YWRkYTQzfGNyZWF0ZWRfYXQ9MjAxOS0wNC0yNFQxMTowMzowMS4zNTQyMDM2MzUrMDAwMFx1MDAyNm1lcmNoYW50X2lkPXIzcHpzM3FjaGd2YjM0NnJcdTAwMjZwdWJsaWNfa2V5PTR0aDN5cDJ0azkzOWt0d2YiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvcjNwenMzcWNoZ3ZiMzQ2ci9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJncmFwaFFMIjp7InVybCI6Imh0dHBzOi8vcGF5bWVudHMuc2FuZGJveC5icmFpbnRyZWUtYXBpLmNvbS9ncmFwaHFsIiwiZGF0ZSI6IjIwMTgtMDUtMDgifSwiY2hhbGxlbmdlcyI6W10sImVudmlyb25tZW50Ijoic2FuZGJveCIsImNsaWVudEFwaVVybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy9yM3B6czNxY2hndmIzNDZyL2NsaWVudF9hcGkiLCJhc3NldHNVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbSIsImF1dGhVcmwiOiJodHRwczovL2F1dGgudmVubW8uc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbSIsImFuYWx5dGljcyI6eyJ1cmwiOiJodHRwczovL29yaWdpbi1hbmFseXRpY3Mtc2FuZC5zYW5kYm94LmJyYWludHJlZS1hcGkuY29tL3IzcHpzM3FjaGd2YjM0NnIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiUWpvYnMiLCJjbGllbnRJZCI6bnVsbCwicHJpdmFjeVVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS9wcCIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwOi8vZXhhbXBsZS5jb20vdG9zIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6dHJ1ZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6dHJ1ZSwibWVyY2hhbnRBY2NvdW50SWQiOiJxam9icyIsImN1cnJlbmN5SXNvQ29kZSI6IkVVUiJ9LCJtZXJjaGFudElkIjoicjNwenMzcWNoZ3ZiMzQ2ciIsInZlbm1vIjoib2ZmIn0=")

    RNMessageChannel.on(TOKEN_RECEIVED, event => {
      //PrintElement(event);
      // if (event.payload.options.creditCard) {

      console.log('register message listerner inside: ', event)

      this.createCreditCardUI(event.payload.clientToken);
      /*  }
      if (event.payload.options.paypal) {
        this.createPaypalUI(event.payload.clientToken);
      } */
    });

    // when the call is made to the braintree purchasing server
    // used to show the user some feedback that the purchase is in process
    RNMessageChannel.on("PAYMENT_PENDING", event => {
      // PrintElement("PAYMENT_PENDING");
      this.setState({ currentPaymentStatus: PAYMENT_FULFILLED });
    });

    // when the purchase succeeds
    // used to show the user some feedback that the purchase has completed successfully
    RNMessageChannel.on(PAYMENT_FULFILLED, event => {
      //PrintElement(PAYMENT_FULFILLED);
      this.setState({ currentPaymentStatus: PAYMENT_FULFILLED });
    });

    // when the purchase succeeds
    // used to show the user some feedback that the purchase has failed to complete
    RNMessageChannel.on(PAYMENT_REJECTED, event => {
      this.setState({ currentPaymentStatus: PAYMENT_REJECTED });
      PrintElement(PAYMENT_REJECTED);
    });

    // PrintElement("registering message listeners - completed");
  };

  /*******************************
   * create the Paypal payment UI element
   */
  createPaypalUI = clientToken => {
    console.log("Not implmented");
  };

  /*******************************
   * create the Braintree UI element
   */
  createCreditCardUI = clientToken => {
    //PrintElement(`createCreditCardUI: ${clientToken}`);

    console.log('clientROken:', clientToken)

    dropin
      .create({
        authorization: clientToken,
        container: "#dropin-container"
      })
      .then(instance => {
        this.setState({ instance });
      })
      .catch(function(err) {

        console.log('error instatnac: ', err)
        // Handle any errors that might've occurred when creating Drop-in
        RNMessageChannel.sendJSON({
          type: "error",
          err
        });
      });
  };
  /***********************************************
   *  handler for when the purchase button is clicke
   */
  handleSubmitPurchaseButtonClicked = () =>  {

    const that = this;
    // PrintElement(`handleSubmitPurchaseButtonClicked: ${this.state.instance}`);
    this.setState({ currentPaymentStatus: "PAYMENT_PENDING" });

    // send a message to the parent WebView so that it
    // can display feedback to user
    RNMessageChannel.emit(RETRIEVE_NONCE_PENDING, {
      payload: {
        type: "success"
      }
    });

    // request a purchase nonce from the Braintree server
    this.state.instance.requestPaymentMethod(function(err, response) {
      if (err) {
        // notify the parent WebView if there is an error
        console.log('nonce rejected')

    that.setState({ currentPaymentStatus: RETRIEVE_NONCE_REJECTED });

        RNMessageChannel.emit(RETRIEVE_NONCE_REJECTED, {
          payload: {
            type: "error",
            err
          }
        });
      } else {
        // pass the nonce to the parent WebView if the purchase is successful
        console.log('nonce fulfill')
        that.setState({ currentPaymentStatus: RETRIEVE_NONCE_FULFILLED });

        RNMessageChannel.emit(RETRIEVE_NONCE_FULFILLED, {
          payload: {
            type: "success",
            response
          }
        });
      }
    });
  };

  handleGoBackButtonSubmit = () => {
    RNMessageChannel.emit("GO_BACK");
  };

  render() {
    console.log('this.s state; ', this.state)
    return (
      <PaymentBackground
        ref={ component => {
          this.webComponent = component;
        }}
      >
        <DropInContainer>
          <div id="dropin-container" />
        </DropInContainer>
        <ButtonContainer>
          {renderIf(this.state.currentPaymentStatus === null || this.state.currentPaymentStatus === "RETRIEVE_NONCE_REJECTED")(
            <Button
              id="submit-button"
              onClick={this.handleSubmitPurchaseButtonClicked}
            >
              Submit
            </Button>
          )}
          {renderIf(this.state.currentPaymentStatus === PAYMENT_FULFILLED)(
            <div>
              <div id="purchase-fulfilled-message">
                Payment Successful!{" "}
              </div>
              {/* <Button onClick={this.handleGoBackButtonSubmit}>
                Return to Shop
              </Button> */}
            </div>
          )}
          {renderIf(this.state.currentPaymentStatus === PAYMENT_REJECTED)(
            <div>
              <div id="purchase-reject-message">
                There was a problem. Click below button and please try again.
              </div>
              <Button onClick={this.handleGoBackButtonSubmit}>
                Return Back
              </Button>
            </div>
          )}
          <div id="messages" />
        </ButtonContainer>
      </PaymentBackground>
    );
  };
}
