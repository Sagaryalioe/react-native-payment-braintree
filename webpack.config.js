const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "/dist"),
        filename: "main.bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    // options: {
                    //     presets: [
                    //         ['env', {
                    //           targets: {
                    //             browsers: ['last 2 versions', 'safari >= 9.3']
                    //           },
                    //         }]
                    //       ],
                    //       plugins: [
                    //         'transform-async-to-generator',
                    //         'transform-class-properties',
                    //     ],
                    // }
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        })
    ]
};